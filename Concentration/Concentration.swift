//
//  Concentration.swift
//  Concentration
//
//  Created by Kirill Ogoltsov on 17/01/2018.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation

class Concentration {
    
    var cards = [Card]()
    var indexOfOnlyOneAndFaceUpCard: Int?
    
    func chooseCard(at index: Int){
        if !cards[index].isFaceUp {
            if let matchIndex = indexOfOnlyOneAndFaceUpCard, matchIndex != index {
                if cards[matchIndex].identifier == cards[index].identifier {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                }
                cards[index].isFaceUp = true
                indexOfOnlyOneAndFaceUpCard = nil
            } else {
                for flipDownIndex in cards.indices {
                    cards[flipDownIndex].isFaceUp = false
                }
                cards[index].isFaceUp = true
                indexOfOnlyOneAndFaceUpCard = index
            }
        }
    }
    
    init(numberOfPairsOfCards: Int) {
        for _ in 0..<numberOfPairsOfCards{
            let card = Card()
            cards += [card, card]
        }
        shuffleTheCards()
    }
    
    func shuffleTheCards() {
        var shuffled = [Card]()
        for _ in 0..<cards.count {
            let rand = Int(arc4random_uniform(UInt32(cards.count)))
            shuffled.append(cards[rand])
            cards.remove(at: rand)
        }
        cards = shuffled
    }
}
