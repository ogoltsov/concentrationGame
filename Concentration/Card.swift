//
//  Card.swift
//  Concentration
//
//  Created by Kirill Ogoltsov on 17/01/2018.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation

struct Card {
    
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    
    init(identifier: Int) {
        self.identifier = identifier
    }
    
    init() {
        self.identifier = Card.getUniqueIdentidier()
    }
    
    static var identifierFactory = 0
    static func getUniqueIdentidier() -> Int {
        identifierFactory += 1;
        return identifierFactory
    }
}
