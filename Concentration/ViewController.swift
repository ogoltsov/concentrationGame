//
//  ViewController.swift
//  Concentration
//
//  Created by Kirill Ogoltsov on 14/01/2018.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var flipCountLabel: UILabel!
    
    @IBOutlet var cards: [UIButton]!
    
    lazy var game = Concentration(numberOfPairsOfCards: (cards.count + 1) / 2)
    
    var flipCount: Int = 0 {
        didSet {
            flipCountLabel.text = "Flips: \(flipCount)"
        }
    }
    @IBAction func touchCard(_ sender: UIButton) {
        if let cardNumber = cards.index(of: sender) {
            game.chooseCard(at: cardNumber)
            self.updateViewFromModel()
        }
        flipCount += 1
    }
    
    func updateViewFromModel() {
        for index in cards.indices {
            let button = cards[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControlState.normal)
                button.backgroundColor = card.isMatched ?  #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 0) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: UIControlState.normal)
                button.backgroundColor = card.isMatched ?  #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 0) : #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1);
            }
            button.isEnabled = !card.isMatched
            if card.isMatched {
                button.setTitle(emoji(for: card), for: UIControlState.normal)
            }
        }
    }
    var emojiArray = ["😎","😍", "😚","😭", "😅", "🤬", "🤢", "💀", "👽", "👹", "💩", "👅", "🤪", "🤯", "🤮", "🤐"]
    var emoji = [Int:String]()
    
    func emoji(for card: Card) -> String {
        if emoji[card.identifier] == nil, emojiArray.count > 0 {
                let randomIndex = Int(arc4random_uniform(UInt32(emojiArray.count)))
                emoji[card.identifier] = emojiArray.remove(at: randomIndex)
            }
        return emoji[card.identifier] ?? "?"
    }
}

